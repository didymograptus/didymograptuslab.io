# Notes to the Rules

A fundamental consideration in developing the Rules has been that potential users of the AGS Format should be able to use standard software tools to produce the data files.

The spreadsheet is the most basic tool for the task and the revised Rules presented in AGS4 simplify the process of creating data from spreadsheet software. Likewise, data files produced according to the Rules can be read directly by spreadsheet software.

Although the Rules make it possible for users to manipulate data files using spreadsheets alone, it is to be expected that more specific software will be used to automate the reading and writing of data files. These software systems may range from simple data entry and edit programs through to complete database systems with AGS Format data import and export capability.

Another fundamental point is that the resulting data file has been designed to be easy to read with minimal computer software. The data files do not replace the printed reports to which they relate, however, the layout does allow data items to be readily identified should the need arise.

The following notes explain some points of detail in the Rules.

## Note i - ASCII 'CSV' Files

The Rules define ASCII data files of a type commonly referred to as Comma Separated Value (CSV). The data items are separated by commas and surrounded by quotes (").

!!! note "Note:"
    Not all software is able to read and write CSV files to the requirements of the AGS Format.

## Note ii - HEADINGs, KEY and REQUIRED Fields

The HEADINGs should be seen as the equivalent of a field name within a database. However, the term HEADING is used within the rules to highlight that this document defines a data transfer format and not a database schema.

KEY FIELDs are important for maintaining data integrity. Without this, the receiving software may not be able to create the inter-relationships within the data in a meaningful way. For the purpose of creating data files, this means that data entered into the combination of KEY FIELDs must be unique in each GROUP and that the corresponding entries are made in the PARENT GROUP where required by [Rule 10c](rules#rule-10c).

REQUIRED Fields ([Rule 10b](rules#rule-10b)) are critical to interpreting the data file. Without data in these fields the user or receiving software may not be able to access the data or process the information within.

Note that there is no requirement to include all HEADINGs in a GROUP. The general approach should be to only include the HEADINGs for which data is required or provided ([Rule 10](rules#rule-10)). This is subject to meeting the requirement to include all KEY and REQUIRED fields ([Rule 10a](rules#rule-10a) and [Rule 10b](rules#rule-10b)).

## Note iii - Units and Data Types

Suggested units of measurement and data types for each of the HEADINGs are given in the [Data Dictionary](datadictionary#data-dictionary). These represent the typical units of measurement that are used in the UK. They will either be the appropriate SI units or the unit defined by the particular Eurocode or British Standard relating to the measurement data under that specific HEADING.

It is recognised that situations will occur where neither the SI unit nor the suggested unit of measurement are appropriate. In these cases, the unit of measurement and/or data TYPE for the results presented may be changed from the one shown in this document and the results presented according to the revised data UNIT / data TYPE.

All entries in the UNIT row must be defined in the UNIT GROUP ([Rule 15](rules#rule-15)). All entries in the TYPE row must be fully defined in the TYPE GROUP ([Rule 17](rules#rule-17)).

## Note iv - Sample Referencing

The SAMP Group has 5 KEY FIELDs which comprise 4 descriptive FIELDs (LOCA\_ID, SAMP\_TOP, SAMP\_TYPE, SAMP\_REF) and a single non descriptive ID (SAMP\_ID).

If descriptive information regarding the sample is not to be disclosed to the data receiver (for example a laboratory), then the single SAMP\_ID field is used and the remaining 4 KEY FIELDs are transmitted as null values. If no such requirement exists then the 4 descriptive fields can be used and the SAMP\_ID can either be transmitted or contain a null value.

This approach is extended to all GROUPs that are descended from SAMP in the [Group Hierarchy](grouphierarchy#group-hierarchy). Laboratory test results may, therefore, be reported using the single or descriptive Key Field options to reference the parent sample depending on what reference system was given to the laboratory.

Samples that have a null LOCA\_ID in the SAMP Group are required to have a null parent entry in the LOCA group when submitted to comply with [Rule 10c](rules.rules#rule-10c).

!!! note "Note:"
    Where these options for sample data exchange are deployed, there may be requirements for additional data acceptance protocols for both data receivers and data producers to ensure that data containing only partial KEY FIELD information can be successfully recombined if data is to be round-tripped.

## Note v - Record Link data TYPE (RL) ([Rule 11](rules#rule-11))

A data TYPE of 'Record Link' appears in the SAMP group (Heading SAMP\_LINK) and provides a method for linking sample data to other data records; in particular the sample source; for example, a monitoring instrument or test that created the sample. This may provide the data receiver with additional information that may be used to interpret testing data related to the sample.

The reference within a Record Link data item is formed using the syntax defined in [Rule 11](rules#rule-11) of:

- GROUP

- The data under the KEY HEADINGs in the order defined in the [Data Dictionary](datadictionary#data-dictionary) (or DICT GROUP for user defined groups)

- Each component of the link being separated by a delimiter of "|" (the pipe character, ASCII Character 124).

For example: "SAMP|BH1|4.50|1|D|UX123"

where SAMP = The Sample data group

BH1 = LOCA\_ID

4.50 = SAMP\_TOP

1 = SAMP\_REF

D = SAMP\_TYPE

UX123 = SAMP\_ID

!!! note "Note:"
    Each value must be presented as it is defined in this document.
	*e.g. the sample top depth is to 2DP*

This facility allows a link to exist between a sample and a monitoring point or other process / test such as the SPT.

Multiple links can be created by concatenation using the separator defined in TRAN\_RCON (default being "+", ASCII Character 43).

**Examples:**

"MONG|BH1|Pipe1" links a sample to the specific monitoring point of its origin.

"ISPT|BH1|4.50+CDIA|BH1|2.00|200" links a sample to the SPT test from which the sample was derived and also references the casing depth at the time of sampling.

The simplicity or complexity of the link depends on user requirements or specification.

## Note vi - Associated files

Where other digital files or file sets are associated with data, the file association should be made with the relevant data type and record.

For example:

- Site location plans would be associated with the PROJ group.

- Photographs of core should be recorded against the core run records within the CORE group.

- Sample logging sheets if included with the data file would be associated with the SAMP group and against the relevant sample.

- Logging files from in situ tests should be associated with the appropriate test group.

## Note vii - Example AGS Format data file

The following is an example of AGS Format. This demonstrates the basics of the format construct and is not complete.

```bash
"GROUP","PROJ"
"HEADING","PROJ_ID","PROJ_NAME","PROJ_LOC","PROJ_CLNT","PROJ_CONT","PROJ_ENG"
"UNIT","","","","","",""
"TYPE","ID","X","X","X","X","X"
"DATA","121415","ACME Gas Works Redevelopment"," Anytown","ACME Enterprises","ACME Drilling Ltd","ACME Consulting"
"GROUP","TRAN"
"HEADING","TRAN_ISNO","TRAN_DATE","TRAN_PROD","TRAN_STAT","TRAN_DESC","TRAN_AGS","TRAN_RECV","TRAN_DLIM","TRAN_RCON"
"UNIT","","yyyy-mm-dd","","","","","","",""
"TYPE","X","DT","X","X","X","X","X","X","X"
"DATA","1","2009-04-01","ACME Drilling Ltd","DRAFT","Draft Logs only","4.0","ACME Consulting","|","+"
"GROUP","TYPE"
"HEADING","TYPE_TYPE","TYPE_DESC"
"UNIT","",""
"TYPE","X","X"
"DATA","ID","Unique Identifier”
"DATA","X,"Text"
"DATA","PA,"Text listed in ABBR Group”
"DATA","DT"," Date time in international format "
"DATA","2DP"," Value; 2 decimal places"
"GROUP","UNIT"
"HEADING","UNIT_TYPE","UNIT_DESC"
"UNIT","",""
"TYPE","X","X"
"DATA","m","metres"
"DATA","yyyy-mm-dd","date"
"GROUP","LOCA"
"HEADING","LOCA_ID","LOCA_NATE","LOCA_NATN"
"UNIT","","m","m"
"TYPE","ID","2DP","2DP"
"DATA","327-16A","523145.00","178456.12"
"GROUP","SAMP"
"HEADING","LOCA_ID","SAMP_TOP","SAMP_REF","SAMP_TYPE","SAMP_ID","SAMP_BASE","SAMP_CONT"
"UNIT","","m","","","","m",""
"TYPE","ID","2DP","X","PA","ID","2DP","X"
"DATA","327-16A","24.55","24","U","ABC121415010","25.00",""

```