# Contract Specification

In order to provide a framework, within which the data can be used, it is necessary to have specifications that fall into the following categories:

-   National Specification

-   General Specification

-   Particular Specification

The National Specification in the UK, the ‘UK Specification for Ground Investigation’[^1], includes the general requirement for data in electronic format. To fully specify the data deliverable requirements, the General clauses for a contract may re-iterate these points and typically Particular Specification clauses should be presented to fully define the data format and deliverable requirements. AGS4 includes many headings for which data can be collected during a geotechnical or geoenvironmental investigation. However, the actual data transferred from the producer to the receiver is described in the particular *CONTRACT SPECIFICATION* between the two parties.

The more precise the information presented in the Particular Specification, the more likely that the data deliverable provided by the supplier will meet expectations. Notes on the approach to development of Particular Specification clauses are presented in a Guidance Document published on the AGS Format website.

AGS4 provides the facility to include user defined groups and headings for specific data requirements. It is important to note that adding additional headings can be very disruptive to producer’s internal processes and may result in considerable extra cost. The specification of additional or user defined fields, therefore, should only be done if absolutely necessary.

[^1]: Site Investigation in Construction: 3 – Specification for ground     investigation. Thomas Telford. 1993.