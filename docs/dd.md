9.2 Data Types
==============

The suggested TYPE of data for each heading is defined in the Data
Dictionary. The abbreviations used on the TYPE row shall be as defined
below and those used in any data file shall be fully defined in the TYPE
group (Rule 17). It should be noted that the TYPE and UNIT selected for
a heading are inter-linked.

+-----------------------+-----------------------+-----------------------+
| **Type**              | **Description**       | **Example / Notes**   |
+-----------------------+-----------------------+-----------------------+
| ID                    | Unique Identifier     | An ID is a unique     |
|                       |                       | identifier used       |
|                       |                       | across the project.   |
+-----------------------+-----------------------+-----------------------+
| PA                    | Text listed in ABBR   | Abbreviations listed  |
|                       | Group                 | in ABBR group. Rule   |
|                       |                       | 16 refers.            |
|                       |                       |                       |
|                       |                       | A list of standard    |
|                       |                       | abbreviations is      |
|                       |                       | available from the    |
|                       |                       | AGS website. Other    |
|                       |                       | abbreviations may be  |
|                       |                       | defined as required.  |
|                       |                       |                       |
|                       |                       | Multiple              |
|                       |                       | abbreviations can be  |
|                       |                       | used in a data        |
|                       |                       | VARIABLE. Where this  |
|                       |                       | occurs, abbreviations |
|                       |                       | are joined using the  |
|                       |                       | concatenation         |
|                       |                       | character defined in  |
|                       |                       | TRAN\_RCON (a "+"     |
|                       |                       | character by          |
|                       |                       | default).             |
+-----------------------+-----------------------+-----------------------+
| PT                    | Text listed in TYPE   | Abbreviations listed  |
|                       | Group                 | in TYPE group. Rule   |
|                       |                       | 17 refers.            |
+-----------------------+-----------------------+-----------------------+
| PU                    | Text listed in UNIT   | Abbreviations listed  |
|                       | Group                 | in UNIT group. Rule   |
|                       |                       | 15 refers.            |
|                       |                       |                       |
|                       |                       | A list of standard    |
|                       |                       | units is available    |
|                       |                       | from the AGS website. |
+-----------------------+-----------------------+-----------------------+
| X                     | Text                  | Abbreviations used in |
|                       |                       | text data are to be   |
|                       |                       | listed in ABBR group. |
|                       |                       | Rule 16 refers.       |
+-----------------------+-----------------------+-----------------------+
| XN                    | Text / numeric        | There are some        |
|                       |                       | measured parameters   |
|                       |                       | that are typically    |
|                       |                       | numeric but can have  |
|                       |                       | a valid result that   |
|                       |                       | is text; examples     |
|                       |                       | include plastic limit |
|                       |                       | (34 or NP) and depth  |
|                       |                       | of water in a         |
|                       |                       | borehole (2.34 or     |
|                       |                       | dry).                 |
|                       |                       |                       |
|                       |                       | Abbreviations used in |
|                       |                       | text data are to be   |
|                       |                       | listed in ABBR group. |
|                       |                       | Rule 16 refers.       |
+-----------------------+-----------------------+-----------------------+
| T                     | Elapsed Time          | e.g. hh:mm:ss         |
+-----------------------+-----------------------+-----------------------+
| DT                    | Date time in          | Ref: ISO 8601:2004    |
|                       | international format  |                       |
|                       |                       | e.g.                  |
|                       |                       |                       |
|                       |                       | yyyy-mm-ddThh:mm:ss.s |
|                       |                       | ssZ(+hh:mm)           |
|                       |                       | or yyyy-mm-dd or      |
|                       |                       | hh:mm:ss or yyyy\     |
|                       |                       | \                     |
|                       |                       | This format is        |
|                       |                       | flexible and can be   |
|                       |                       | used in full or part  |
|                       |                       | according to user     |
|                       |                       | requirements          |
+-----------------------+-----------------------+-----------------------+
| MC                    | British Standard      |                       |
|                       | BS1377 : Part 2       |                       |
|                       | reported moisture     |                       |
|                       | content               |                       |
+-----------------------+-----------------------+-----------------------+
| *n*DP                 | Value with required   | e.g. 2DP = 2 decimal  |
|                       | number of decimal     | places = 2.34         |
|                       | places                |                       |
+-----------------------+-----------------------+-----------------------+
| *n*SF                 | Value with required   | e.g. 2SF = 2          |
|                       | number of significant | significant figures = |
|                       | figures               | 1.2, 10               |
+-----------------------+-----------------------+-----------------------+
| *n*SCI                | Scientific Notation   | e.g. 73100 as 2SCI =  |
|                       | with required number  | 7.31E4; 73100 as 1SCI |
|                       | of decimal places     | = 7.3E4               |
+-----------------------+-----------------------+-----------------------+
| U                     | Value with a variable | This is used for      |
|                       | format                | fields that contain   |
|                       |                       | values with differing |
|                       |                       | accuracy; e.g.        |
|                       |                       | ERES\_RVAL.           |
+-----------------------+-----------------------+-----------------------+
| DMS                   | Degrees:Minutes:Secon | e.g. 51:28:52.498     |
|                       | ds                    |                       |
+-----------------------+-----------------------+-----------------------+
| YN                    | Yes or No             | Data in the file will |
|                       |                       | be either a *Y* or    |
|                       |                       | *N*                   |
|                       |                       |                       |
|                       |                       | e.g. Y                |
|                       |                       |                       |
|                       |                       | These fields if       |
|                       |                       | included in a data    |
|                       |                       | group should include  |
|                       |                       | data to prevent any   |
|                       |                       | possible              |
|                       |                       | mis‑interpretation.   |
+-----------------------+-----------------------+-----------------------+
| RL                    | Record Link           | Rule 11 refers.       |
|                       |                       |                       |
|                       |                       | Text in specified     |
|                       |                       | format that refers to |
|                       |                       | one or more records   |
|                       |                       | in other Groups by    |
|                       |                       | Key fields.           |
+-----------------------+-----------------------+-----------------------+

9.3 Units of Measurement
========================

The suggested units for each heading are provided in the Data
Dictionary. The suggested units are reflective of specifications and
standards used in the UK.
