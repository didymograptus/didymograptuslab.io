# AGS4 Changelog

## Summary of rule changes from AGS3 to AGS4

- The full ASCII character set now permitted ([Rule 1](rules#rule-1)).

- The rule limiting line length to 240 characters in previous versions of the AGS Format has been removed together with the requirement for continuation &lt;CONT&gt; lines.

- \*\* and \* prefixes to group names and headings removed.

- ? prefix is no longer required for user defined groups or headings.

- All lines now to include leading data descriptors ([Rule 3](rules#rule-3))

    -    "GROUP"

    -   "HEADING"

    -   "UNIT"

    -   "TYPE"

    -   "DATA"

-   Group headings should also be ordered in accordance with the groups in the data dictionary document (this document, Section 8) with the headings for the UDFs (User Defined Fields) following in the order defined in the DICT GROUP- [Rule 4](rules#rule-4) and [Rule 5](rules#rule-5).

## CODE Group

- CODE has been removed and its contents included in the ABBR group.

## Data Types and Heading Status

- Although all entries in a transfer file are still alpha-numeric the concept of “Data types” are now included. Of particular interest are:

- The special data type for moisture content (MC) and that this changes with the value as required by the appropriate BS.

- The Record Link data type that allows cross-hierarchical links between data in groups. Currently this is only in use in the SAMP Group.

- International date/time format fields added or amended. (yyyy-mm-ddThh:mm:ss.sssZ(+hh:mm))

- The concept of KEY, REQUIRED and OTHER has been included. KEY fields are those which are needed to uniquely define the data entry, whilst REQUIRED fields are those attributes which are needed to interpret the file and its contents.

## LOCA Group

- The HOLE group renamed LOCA. This is to emphasize that not all samples and observations are made in boreholes. The acronym HOLE (Hole Or Location Equivalent) has been replaced with the more logical LOCA.

## SAMP Group

- This includes all samples taken in the field, laboratory samples, including those prepared for individual tests, derived from another sample or group of samples, or samples taken for monitoring.

- Sample Key Fields have been extended to include SAMP\_ID, a unique identifier which can be used as required. This can be used to develop systems where the samples are referenced by bar codes. Those using the present AGS4 Key Fields described in AGS 3.1 to define their samples can continue to do so.

- Each and every sample will have a unique key set from (LOCA\_ID, SAMP\_TOP, SAMP\_REF, SAMP\_TYPE, and SAMP\_ID) following the usual AGS rules.

- SAMP\_LINK has been added to enable samples to be linked to other tests or observations. The format of the SAMP\_LINK is group|keyheading|keyheading e.g. MONG|BH1|Pipe1 (where the delimiters are defined in TRAN\_DLIM as vertical bar characters “|”).

- For monitoring, the sample will still need a unique key set from the above list, which could be SAMP\_ID. It can be linked to MONG using the SAMP\_LINK.

## ERES Group

- A new group ERES has been included for environmental testing to replace CNMT which was considered to be insufficient for current environmental testing and the extended data requirements associated with more complex testing records.

## GCHM Group

- A new group GCHM has been added for geotechnical chemistry testing which is considered to be different to the Environmental testing reported in ERES. This group has been prepared to contain all the data required for aggressivity testing of soil and water in accordance with British Standard 1377-3 and BRE Special Digest 1.

## Rock Testing Groups

Each rock test now has its own group:

- RCCV - Chalk Crushing Value Tests
- RELD - Relative Density Tests
- RDEN - Rock Porosity and Density Tests
- RPLT - Point Load Testing
- RSCH - Schmidt Rebound Hardness Tests
- RSHR - Shore Scleroscope Hardness Tests
- RTEN - Tensile Strength Testing
- RUCS - Rock Uniaxial Compressive Strength and Deformability Tests
- RWCO - Water Content of Rock Tests

Materials / Aggregate testing is reported separately with each test having its own group:

- AAVT - Aggregate Abrasion Tests
- ACVT - Aggregate Crushing Value Tests
- AELO - Aggregate Elongation Index Tests
- AFLK - Aggregate Flakiness Tests
- AIVT - Aggregate Impact Value Tests
- ALOS - Los Angeles Abrasion Tests
- APSV - Aggregate Polished Stone Tests
- ARTW - Aggregate Determination of the Resistance to Wear (micro-Deval)
- ASDI - Slake Durability Index Tests
- ASNS - Aggregate Soundness Tests
- AWAD - Aggregate Water Absorption Tests

## Soil Testing Groups

Each Soil test now has its own group:

- CBRG - California Bearing Ratio Tests - General
- CBRT - California Bearing Ratio Tests - Data
- CMPG - Compaction Tests - General
- CMPT - Compaction Tests - Data
- CONG - Consolidation Tests - General
- CONS - Consolidation Tests - Data
- ESCG - Effective Stress Consolidation Tests - General
- ESCT - Effective Stress Consolidation Tests - Data
- FRST - Frost Susceptibility Tests
- GCHM - Geotechnical Chemistry Testing
- GRAG - Particle Size Distribution Analysis - General
- GRAT - Particle Size Distribution Analysis - Data
- LDEN - Density Tests
- LDYN - Dynamic Testing
- LLIN - Linear Shrinkage Tests
- LLPL - Liquid and Plastic Limit Tests
- LNMC - Moisture Content Tests
- LPDN - Particle Density Tests
- LPEN - Laboratory Hand Penetrometer Tests
- LSLT - Shrinkage Limit Tests
- LSTG - Initial Consumption of Lime Tests - General
- LSTT - Initial Consumption of Lime Tests - Data
- LSWL - Swelling Index Testing
- LVAN - Laboratory Vane Tests
- MCVG - MCV Tests - General
- MCVT - MCV Tests - Data
- PTST - Laboratory Permeability Tests
- RELD - Relative Density Tests
- SHBG - Shear Box Testing - General
- SHBT - Shear Box Testing - Data
- SUCT - Suction Tests
- TNPC - Ten Per Cent Fines
- TREG - Triaxial Tests - Effective Stress - General
- TRET - Triaxial Tests - Effective Stress - Data
- TRIG - Triaxial Tests - Total Stress - General
- TRIT - Triaxial Tests - Total Stress - Data

1.  Effective stress testing.

Triaxial effective stress results groups TREG & TRET have been added.
Effective Stress Consolidation testing added in groups ESCG and ESCT.

1.  **Monitoring Installations & Readings Groups**

- MONP has been renamed MONG and MONR has been replaced with a new group MOND (Monitoring test Data) with paired results fields so that readings from any instrument type can be recorded.

- PIPE group added to transfer details of instrumentation installations.

## In situ Testing Groups

- PLTG & PLTT have been added to report plate bearing testing to British Standard 1377 part 9 and EN1997‑2.

- DCPG & DCPT have been included to record dynamic cone penetrometer testing (the ‘TRL probe’) separate from dynamic probing.

## Drilling Observations Groups

- DOBS has been added to allow transfer of drilling observations. The group provides facility for instrumented drilling measurements to be transferred where this is required.

- WADD has been included to allow for details of addition of water to exploratory holes.


## Summary of Amendments to Version 4.0

## October 2011

- Section 7 added to provide notes on Internationalization of the AGS Format.

- Note on associated files added to Notes on rules.

- Section 9.3 Units of Measurement section updated/clarified.

- Ø characters in heading names corrected to 0 characters.

- uS/cm corrected to μS/cm (LRES and SCPT groups).

- Specimen depth fields amended to “Depth of top of specimen” to clarify the required data.

- \_REM heading added to CMPT, DETL, FRAC, GEOL, GRAT, MCVT, PLTT, PMTL, SCDT, TRET and TRIT groups.

- \_TESN heading added to laboratory testing groups. To allow for test  reference data to be included.

- CBRG notes for guidance corrected.

- CHOC\_REF updated to a key field (CHOC group).

- CMPG\_TESN added to CMPG and CMPT as key

- Description of FILE\_FSET updated in group descriptions to provide examples of the types of files that could be associated with the data.

- DICT\_UNIT type corrected to PU (DICT group).

- HDPH group notes for guidance amended.

- HORN group added to include exploratory hole orientation and inclination data. Same removed from HDPH group.

- IPID\_RES suggested type changed to 2DP.

- Example added for IRES\_TYPE

- Example for ISPT\_METH updated.

- ISPT notes for guidance extended.

- LNMC group title amended to Moisture Content Test. LNMC\_ISNT added. Notes for guidance updated.

- LRES\_WCND changed to LRES\_WRES to express results as water resistivity in ohm m.

- LSLT\_MCI added.

- MOND group extended to include measurement methods, limits and accreditation information (Headings MOND\_METH, MOND\_CRED, MOND\_LIM, MOND\_ULIM added).

- MONG\_BRGC example corrected

-   Heading PMTL\_RRM corrected to PMTL\_REM (PMTL group).

- PTST\_TYPE type changed to PA and example updated.

- RCCV\_TESN included as key

- Heading RCCV\_10 corrected to RCCV\_100 (RCCV group).

- RDEN\_TEMP added.

- RWCO\_TEMP added.

- SCDG\_CH and SCDG\_CHMT added

- SCPG\_TYPE example corrected.

- SCPT\_DPR removed.

- TREM\_ETIM added.

- Index extended.

- Appendix 3 (‘Notes on Specifying AGS Format Data Deliverables’) removed.

## February 2017

- ‘Moisture content’ has been replaced with the term ‘Water content’. There is no provision for an additional heading for this data. Instead, the data type for every moisture content heading has been changed from ‘MC’ to ‘X’ to allow transmission of results both in the pre-existing and new form to be carried in a file:

- PMTD\_SEQ heading in the ‘Pressuremeter Test Results – Individual Loops’ (PMTL) group is marked for deletion. It must remain in each AGS 4.0.4 submission as a NULL heading (see Rule 12).

- A National datum heading LOCA\_NATD has been added to the LOCA group.

- LOCA\_ORID, LOCA\_ORJO, LOCA\_ORCO have been added to facilitate combined archived project data in a data submission.

- A new heading to record the length of a sample SAMP\_RECL has been added to the SAMP group.

- Fracture spacing headings have a new data type of ‘XN’ to allow both numeric and text data to be transmitted. The following headings have been changed - FRAC\_FI, FRAC\_IAVE, FRAC\_IMAX.

- CHOC\_REF data type has been changed from ID to ‘X’. An ‘ID’ data type implied a uniqueness throughout the entire data submission.

- A new heading RDEN\_IDEN for Intact Dry Density has been added to the RDEN group.

- RPLT\_PLS and RPLT\_PLSI data type changed to 2DP from 1DP.

- TRIT\_CU data type changed to 0DP from 2SF.

- Window Sampling amended to Dynamic sampling throughout document.

- IPID\_RES data type changed to XN to allow both text and numeric data to be reported.

- Two new headings have been added to GCHM. GCHM\_DLM for the detection limit and GCHM\_RTXT for the reported value.