# ACKNOWLEDGEMENTS

This document has been prepared by the Association of Geotechnical and Geoenvironmental Specialists with the encouragement and support of the working party members. The acknowledges the generous time and resources given to the project by the individual members and their employers. Without their enthusiastic support this ongoing project would not be possible.

Comment and feedback from the wider geotechnical and geoenvironmental industry has also been fundamental to the ongoing evolution of the Format, ensuring that the needs of the industry and its clients continue to be met.

**Working Party Members:**

-   Katie Aguilar - *Bentley Systems*

-   Romain Arnould - *Fugro*

-   Mark Bevan - *Structural Soils*

-   Jackie Bland (chair) - *Geotechnics*

-   Neil Chadwick - *Arup*

-   Jerome Chamfray - *Atkins*

-   Roger Chandler - *Keynetix*

-   Tony Daly - *Amageo Limited*

-   David Entwisle - *British Geological Survey*

-   David Farmer - *Arup*

-   Ian Joyce - *Bentley Systems*

-   Ian Linton - *Soil Engineering Geoservices Ltd*

-   Simon Miles - *Atkins*

-   Mike Palmer - *CH2M*

-   Christopher Power - *Mott MacDonald*

-   Syd Pycroft - *Blue Anchor Ground Engineering Ltd*

-   Felicity Sims - *CH2M*

-   Leonard Threadgold - *Geotechnics*

-   Sarah Valentine - *ESG*

-   Stephen Walthall

-   Benjamin Wood - *British Geological Survey*

