# UPDATING

To meet the rapidly changing needs of its users the AGS Format must continue to develop. The publication of a First Edition (1992) and a Second Edition (1994), were in hard copy forms. However, the broadening of the user base has required greater flexibility for dissemination of amendments. The AGS website has been used since the Third Edition (2004) to publish subsequent updates. Whilst placing the Format in open access on the website permits more frequent updates, all changes are subject to rigorous control and notification procedures.

Extensions to the AGS Format will continue to be necessary from time to time and details are given in Appendix 1 of how updates are notified to the user community.

Any problems in the use of this document should be brought to the attention of the AGS via the discussion board on the website. Problems with proprietary software should be directed to the suppliers.
