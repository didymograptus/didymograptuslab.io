# Publication History


|Edition Reference|Date of issue|Amendments                                  |
|-----------------|-------------|--------------------------------------------|
|03/92            |March 1992   |Original Issue                              |
|07/94            |July 1994    |Rules, Appendix 1, Appendix 2 and Appendix 3|
|3                |November 1999|                                            |
|AGS-M            |March 2002   |                                            |
|3.1              |December 2004|Incorporated AGS-M                          |
|3.1a             |March 2005   |Addendum to 3.1                             |
|4.0              |May 2010     |                                            |
|4.0.1            |March 2011   |Addendum to 4.0                             |
|4.0.2            |May 2011     |Addendum to 4.0                             |
|4.0.3            |October 2011 |Addendum to 4.0                             |
|4.0.4            |February 2017|For amendments see Appendix 3               |
