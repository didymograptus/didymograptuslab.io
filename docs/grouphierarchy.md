# Group Hierarchy

AGS4 Groups are organised in a hierarchy with an inverted tree like structure. At the top of the tree is the PROJ Group, with the majority of other Groups below this.

One of the Groups immediately below PROJ is Location Details (LOCA). All the in situ testing data lies directly below LOCA; for example SPT results in the ISPT Group. LOCA is termed the ‘parent’ Group of ISPT and ISPT is termed a ‘child’ Group of LOCA. The parent group of all the laboratory testing is sample data (SAMP).

Each Group has only one parent defined in the Hierarchy, but there can be many Groups below each parent. Each Group is linked to its parent (the Group above it in the hierarchy) by the Key Fields. Equally, each Group is linked to the Group(s) below it by Key Fields. For this structure to work, and the link to be made correctly between related Groups, the data in the Key Fields must be consistent and unique. If a data Group is included in an AGS submission, its Parent Group must also be included [Rule 10c](rules#rule-10c), this applies all the way up to the top of the tree. Therefore, for example, the SAMP Group must always be present in the submission if there is any triaxial testing included in the TRIG group.

The following table defines the Group hierarchy by indicating the parent for each Group. The Key Fields that create the link between these Groups are indicated in the [Data Dictionary Section](datadictionary#data-dictionary).

It should be noted that there are nine Groups that are not part of this hierarchy and relate to the data file submission and description. The PROJ, TRAN, ABBR, TYPE, DICT, FILE, UNIT, LBSG and STND Groups each have a general purpose to describe the contents of the data file as follows:

- The PROJ, TRAN, ABBR, TYPE and UNIT Groups must always be included in an AGS file as they define the project, the submission details and the abbreviations, data types & units used within the data file.

- The DICT Group must be included if any user defined Groups or Headings are present.

- The FILE Group must be included if any associated files (non-AGS format files) are included in the submission ([Rule 20](rules#rule-20)).

- The STND Group may be used to list the various standards and specifications that define the methods by which the data has been collected.

- LBSG may be used to list test scheduling references used on a project.